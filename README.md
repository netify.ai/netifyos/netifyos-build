# NetifyOS Build Script

## Description

This project provides the tools to build the NetifyOS ISO and version tree.

## Version Branches

The master branch is not used in the NetifyOS project.  Instead, the version branches track the current stable development:

* netifyos7
* netifyos8
* and beyond
